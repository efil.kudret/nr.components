# NrSelect

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

## Code scaffolding

Run `ng generate component component-name --project nr-select` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project nr-select`.
> Note: Don't forget to add `--project nr-select` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build nr-select` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build nr-select`, go to the dist folder `cd dist/nr-select` and run `npm publish`.

## Running unit tests

Run `ng test nr-select` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
