import {NRSelectDataSource} from './nr-select-data-source';

export class NRSelectOptions {
  public allowClear?: boolean;
  public searchable?: boolean;
  public displayMember?: string;
  public valueMember?: string;
  public hideLabel?: boolean;
  public dataSource?: NRSelectDataSource;
  public loading?: boolean;
  public useAngularForm?: boolean;
  public lazyLoad?: boolean;
  public displayTextFormat?: any;
  public searchAfterKeypress? : boolean;
  public disabled?: boolean;
}
