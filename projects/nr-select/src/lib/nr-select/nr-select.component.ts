import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import * as _ from 'lodash';
import {NRSelectOptions} from '../model/nr-select-options';

@Component({
  selector: 'nr-select',
  templateUrl: './nr-select.component.html'
})
export class NrSelectComponent implements OnInit {

  dropdownId: string;
  containerId: string;
  searchBoxId: string;

  safeCheck = {
    loading: undefined,
    disabled: undefined,
    searchAfterKeypress: undefined
  };

  constructor(private cdRef: ChangeDetectorRef) {
    this.searchKeyChanged
      .pipe(debounceTime(500))
      .subscribe(v => {
        this.searchKey = v;
        if (this.options && this.options.displayMember) {
          this.search();
        }
      });
    this.dropdownId = Math.random().toString().replace('.', '');
    this.containerId = 'container-' + this.dropdownId;
    this.searchBoxId = 'search-box-' + this.dropdownId;
  }

  @Input() editable: boolean = true;

  @Input() form: FormGroup;
  @Input() controlName: string;
  @Input() label: string;
  @Input() maxLength: number;
  @Input() data: any[];
  @Input() options: NRSelectOptions;

  selectedValueChanged: Subject<any> = new Subject<any>();
  getSelectedInvoked: boolean = false;
  _submitted?: any = undefined;
  _value?: any = undefined;
  notFound: boolean = undefined;
  dropdownVisible: boolean = false;
  selected: any;
  searchKey: string = '';

  @Output() valueChange = new EventEmitter<any>();
  @Output() searchKeyChanged: Subject<string> = new Subject<string>();
  @Output() dataLoaded: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSelected: EventEmitter<any> = new EventEmitter<any>();
  @Output() submittedChange = new EventEmitter<number>();

  @ViewChild('searchInput') searchInput: ElementRef;


  @Input()
  public get submitted() {
    return this._submitted;
  }

  public set submitted(val) {
    this._submitted = val;
    this.submittedChange.emit(val);
  }

  @Input()
  public get value() {
    return this._value;
  }

  public set value(val) {
    let different = val != this._value;
    this._value = val;
    this.checkSelected();
    if (different) {
      this.selectedValueChanged.next(this._value);
    }
  }

  getValueMemberKey() {
    return this.options && this.options.valueMember ? this.options.valueMember : 'id';
  }

  findInData() {
    return _.find(this.data, (x) => {
      return x[this.getValueMemberKey()] == this._value;
    });
  }

  setSelected() {
    let currentSelected = this.findInData();
    if (currentSelected) {
      this.selected = currentSelected;
      currentSelected.current = true;
      this.setSelectedScrollPosition();
    } else {
      this.selected = undefined;
    }
  }

  checkSelected() {
    if (this.options && this.options.dataSource && this._value) {
      if (!this.getSelectedInvoked) {
        this.getSelectedInvoked = true;
        this.dataLoad(this.value).then(r => {
            this.setSelected();
            this.getSelectedInvoked = false;
          }
        );
      }
    } else {
      this.setSelected();
    }
  }


  search() {
    if (this.options.dataSource) {
      this.dataLoad().then(r => {
        this.setSelected();
      });
    } else {
      _.each(this.data, (x) => {
        x.hide = _.lowerCase(x[this.options.displayMember]).indexOf(_.lowerCase(this.searchKey)) < 0;
      });
      let results = _.filter(this.data, (x) => {
        return !x.hide;
      });
      this.notFound = (results == undefined || results.length == 0);
    }
  }

  private dataLoad(withSelectedVal = undefined) {
    return new Promise((resolve, reject) => {
      if (this.options && this.options.dataSource) {
        this.safeCheck.loading = true;
        if (this.options.dataSource) {
          this.options.dataSource.loader(this.options.dataSource.parent, this.searchKey, withSelectedVal).subscribe(r => {
            this.safeCheck.loading = false;
            if (_.isArray(r)) {
              this.data = r;
            } else {
              this.data = r.data;
            }
            this.dataLoaded.emit();
            resolve(1);
          }, err => {
            console.log('nr-select-data-load-error', err);
            reject();
          });
        }
      }
    });
  }

  ngOnInit() {
    this.safeCheck = {
      disabled: this.safetyCheckOption('disabled'),
      loading: this.safetyCheckOption('loading'),
      searchAfterKeypress: this.safetyCheckOption('searchAfterKeypress')
    };

    this.checkSelectedValue();
    if (this.options && this.options.dataSource) {
      this.options.dataSource.reload = () => {
        return this.dataLoad(this.value).then(r => this.setSelected());
      };
      this.options.dataSource.clearSelected = () => {
        this.clearValue(false);
      };
      if (!this.options.lazyLoad) {
        this.options.dataSource.reload();
      }

      if (this.value) {
        this.dataLoad(this.value).then(r => this.setSelected());
      }
    } else if (this.data) {
      this.checkSelected();
    }
    this.selectedValueChanged.subscribe(val => {
      this.valueChange.emit(val);
    });
    this.dataLoaded.subscribe(x => {
      this.checkSelected();
    });
  }

  checkSelectedValue() {
    if (this._value && !this.options.dataSource) {
      let selected = _.find(this.data, (x) => {
        return x.id == this._value;
      });
      if (selected && this._value != selected) {
        this.select(selected, false);
      }
    } else {

    }
  }

  safetyCheckOption(optionKey) {
    if (!this.options) {
      return false;
    }
    if (this.options && this.options[optionKey]) {
      return true;
    } else {
      return false;
    }
  }

  toggleDropdown() {
    if (this.editable) {
      if (this.options && this.options.disabled) {
        return;
      }

      let newVisibleValue = !this.dropdownVisible;
      if (this.safeCheck.searchAfterKeypress) {
        this.dropdownVisible = newVisibleValue;
      } else {
        if (this.options.lazyLoad) {
          this.options.dataSource.reload();
        }
        if (newVisibleValue && this.options && this.options.dataSource) {
          this.dataLoad().then(r => {
            this.dropdownVisible = newVisibleValue;
            this.setSelected();
          });
        } else if (this.selected && newVisibleValue) {
          this.dropdownVisible = newVisibleValue;
          this.setSelectedScrollPosition();
        } else {
          this.dropdownVisible = newVisibleValue;
        }
      }
      this.cdRef.detectChanges();
      if (newVisibleValue) {
        let searchElem = document.getElementById(this.searchBoxId);
        if (searchElem) {
          searchElem.focus();
        }
      }

      if (!this.options.dataSource && this.data) {
        _.each(this.data, (x) => {
          x.hide = false;
        });
      }
    }
  }

  setSelectedScrollPosition() {
    // this.cdRef.detectChanges();
    // let dropdown = document.getElementById(this.dropdownId);
    // if (dropdown) {
    //   let ul = dropdown.children[0];
    //   let currentElem = _.find(ul.children, (x) => {
    //     return _.filter(x.classList, (y) => {
    //       return y == 'current'
    //     }).length != 0;
    //   })
    //   if (currentElem) {
    //     let p = currentElem.getBoundingClientRect();
    //     let dropDownItem = document.getElementById(this.dropdownId);
    //     let containerItem = document.getElementById(this.containerId);
    //     let searchItem = document.getElementById(this.searchBoxId);
    //     if (dropDownItem) {
    //
    //       if (containerItem.offsetParent.nodeName != 'BODY')
    //         containerItem = (containerItem.offsetParent as HTMLElement);
    //
    //       let v1 = containerItem.offsetTop;
    //       let v2 = dropDownItem.offsetTop;
    //       let v3 = searchItem.offsetHeight;
    //
    //       dropDownItem.scrollTop = p.top - (v1 + v2 + v3 + 30);
    //     }
    //   }
    // }
  }

  getDisplayText(item) {
    if (!item) {
      item = this.selected;
    }
    if (this.options && this.options.displayTextFormat) {
      return this.options.displayTextFormat(item);
    } else if (this.options && this.options.displayMember) {
      return item[this.options.displayMember];
    } else {
      return item.text;
    }
  }

  clearCurrentValues() {
    _.each(this.data, (x) => {
      x.current = false;
    });
  }

  select(item, toggle) {
    this.clearCurrentValues();
    item.current = true;

    if (this.searchKey != '') {
      this.searchKey = '';
    }
    this.selected = item;
    if (this.options && this.options.valueMember) {
      this.value = item[this.options.valueMember];
    } else {
      this.value = item.id ? item.id : item;
    }
    if (toggle) {
      this.toggleDropdown();
    }
    this.onSelected.emit(item);
  }

  clearValue(toggle = true) {
    this.value = undefined;
    this.valueChange.emit(this.value);

    if (this.dropdownVisible) {
      this.toggleDropdown();
    }

    // if (toggle) {
    //   this.toggleDropdown();
    // }
    this.onSelected.emit(undefined);
  }

  onSearchKeyChange() {
    this.searchKeyChanged.next(this.searchKey);
  }

  get invalid() {
    if (this.options) {
      if (!this.submitted || !this.form || !this.form.controls[this.controlName]) {
        return false;
      } else {
        return this.form.controls[this.controlName].errors;
      }
    } else {
      return false;
    }
  }

}
