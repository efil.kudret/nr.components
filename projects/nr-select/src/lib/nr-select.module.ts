import { NgModule } from '@angular/core';
import { NrSelectComponent } from './nr-select/nr-select.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
@NgModule({
  declarations: [NrSelectComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule
  ],
  exports: [NrSelectComponent]
})
export class NrSelectModule { }
