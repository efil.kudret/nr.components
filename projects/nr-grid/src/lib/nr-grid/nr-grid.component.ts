import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {NRGridButton} from './model/button/nr-grid-button';
import {NRGridColumn} from './model/column/nr-grid-column';
import * as _ from 'lodash';
import {NRGridOptions} from './model/options/nr-grid-options';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {NgbDate, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NRGridColumnType} from './model/column/nr-grid-column-type';
import {FilterType} from './model/filter/filter-type';
import format from 'date-fns/format';
import {NRSelectOptions} from 'nr-select';

@Component({
  selector: 'nr-grid',
  templateUrl: './nr-grid.component.html'
})
export class NRGridComponent implements OnInit {

  activeDeleteModal;

  @Input() options: NRGridOptions;
  @Input() loading: boolean;
  @Output() onPageChanged = new EventEmitter<number>();
  @Output() public onEditClicked = new EventEmitter<any>();
  @Output() public onDeleteClicked = new EventEmitter<any>();
  @Output() public onFilterChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() public onOrderChanged = new EventEmitter<any>();
  @Output() public onRowClicked = new EventEmitter<any>();
  @Output() public onRowValueChanged = new EventEmitter<any>();
  @Output() public onCellLinkClicked = new EventEmitter<any>();
  @Output() public onLimitChanged = new EventEmitter<any>();
  @Output() public onCustomButtonClicked = new EventEmitter<any>();

  @ViewChild('deleteModal', {static: false}) private deleteModal;
  @ViewChild('rangePicker', {static: false}) private rangePicker;

  @Input() columnHeaderTemplate: TemplateRef<any>;
  @Input() footerTemplate: TemplateRef<any>;

  columns: NRGridColumn[];
  editingRows: any = {};

  limitChangeOptions: NRSelectOptions = {
    searchable: false,
    valueMember: 'id',
    displayMember: 'text',
    hideLabel: true
  };
  pages =
    [
      {id: 10, text: '10'},
      {id: 25, text: '25'},
      {id: 50, text: '50'},
      {id: 100, text: '100'},
    ];

  go: NRGridOptions;

  ranges: any = {};
  filterChanged: Subject<any> = new Subject<any>();

  _rowFilter: any;

  @Input() get rowFilter() {
    return this._rowFilter;
  }

  set rowFilter(value) {
    this.changePage(1);
    this._rowFilter = value;
    this.rowFilterChange.emit(this._rowFilter);
  }

  @Output() rowFilterChange = new EventEmitter<any>();
  @Output() dataLoaded = new EventEmitter<any>();

  changePage(page) {
    if (this.options) {
      this.options.page = page;
    }
  }

  order: any = {};
  deleteData: any;

  constructor(private modalService: NgbModal, private cdr: ChangeDetectorRef) {
    this.filterChanged.pipe(debounceTime(500)).subscribe(val => {
      if (this.go.dataSource) {
        this.go.dataSource.reload();
      } else {
        this.onFilterChanged.emit(this.rowFilter);
      }
    });
  }

  filteredColumns(): NRGridColumn[] {
    const f = _.filter(this.go.columns, (x) => {
      return !x.denied;
    });
    return _.filter(f, (x) => {
      return !x.hide;
    });
  }

  private dataLoad(go: NRGridOptions) {
    go.loading = true;
    if (go.dataSource.usePromise) {
      go.dataSource.loader(go.dataSource.parent, this.rowFilter)
        .then(r => {
          go.data = r.data;
          go.rowCount = r.rowCount;
          go.loading = false;
          this.dataLoaded.emit(true);
          if (r.summary) {
            go.summary = r.summary;
          }
        })
        .catch(e => {
          go.loading = false;
          console.log('qr-grid-data-load-error', e);
        });
    } else {
      go.dataSource.loader(this.go.dataSource.parent, this.rowFilter).subscribe(r => {
        go.data = r.data;
        go.rowCount = r.rowCount;
        go.loading = false;
        this.dataLoaded.emit(true);
        if (r.summary) {
          go.summary = r.summary;
        }
      }, err => {
        go.loading = false;
        console.log('qr-grid-data-load-error', err);
      });
    }
  }

  ngOnInit() {

    if (!this.rowFilter) {
      this.rowFilter = {};
    }

    if (this.options && !this.options.limit) {
      this.options.limit = 10;
    }

    if (this.options && !this.options.loading) {
      this.options.loading = true;
    }

    if (this.options && this.options.pager === undefined) {
      this.options.pager = true;
    }

    this.go = this.options;

    if (!this.go.data) {
      this.go.data = [];
    }
    if (!this.go.page) {
      this.go.page = 1;
    }
    if (!this.go.limit) {
      this.go.limit = 10;
    }
    if (!this.go.rowCount) {
      this.go.rowCount = 0;
    }

    if (!this.go.bootstrapTableClass) {
      this.go.bootstrapTableClass = 'table-bordered';
    }

    this.columns = this.filteredColumns();

    this.loadDropdownData();
    if (this.go.dataSource) {
      this.go.dataSource.reload = () => {
        this.editingRows = {};
        return this.dataLoad(this.go);
      };
      if (!this.go.dataSource.manualLoading) {
        this.go.dataSource.reload();
      } else {
        this.go.loading = false;
      }
    }
  }

  loadDropdownData() {
    const my = this;
    _.each(this.go.columns, (x) => {
      if (x.dropdownDataService) {
        my.loadLookupData(x);
      }
    });
  }

  filterChange() {
    this.changePage(1);
    this.filterChanged.next(this.rowFilter);
    this.loadDropdownData();
  }

  pageChange(val) {
    if (this.go.dataSource) {
      this.go.page = val;
      this.go.dataSource.reload();
    }
    this.onPageChanged.emit(val);
  }

  editClicked(rowData) {
    this.onEditClicked.emit(_.clone(rowData));
  }

  checkEditButtonOption(rowData) {
    if (!this.go.actionButtonsOptions) {
      return true;
    } else if (!this.go.actionButtonsOptions.edit) {
      return false;
    }

    if (typeof this.go.actionButtonsOptions.edit == 'boolean') {
      return this.go.actionButtonsOptions.edit;
    } else {
      return this.go.actionButtonsOptions.edit(rowData);
    }
  }

  checkDeleteButtonOption(rowData) {
    if (!this.go.actionButtonsOptions) {
      return true;
    } else if (!this.go.actionButtonsOptions.delete) {
      return false;
    }

    if (typeof this.go.actionButtonsOptions.delete == 'boolean') {
      return this.go.actionButtonsOptions.delete;
    } else {
      return this.go.actionButtonsOptions.delete(rowData);
    }
  }


  deleteClicked(rowData) {
    this.activeDeleteModal = this.modalService.open(this.deleteModal, {centered: true});
    this.deleteData = rowData;
  }

  submitDelete() {
    this.onDeleteClicked.emit(this.deleteData);
    this.activeDeleteModal.close();
  }

  linkClicked(data) {
    this.onCellLinkClicked.emit(data);
  }

  isDefaultCell(t: NRGridColumnType) {
    return !t || (t == NRGridColumnType.default || t == NRGridColumnType.inlineEditor);
  }

  isLinkCell(t: NRGridColumnType) {
    return t === NRGridColumnType.link;
  }

  toggleOrder(column) {
    const columnKey = column.orderKey ? column.orderKey : column.name;
    if (this.order.column === columnKey) {
      this.order.desc = !this.order.desc;
    } else {
      this.order.desc = false;
    }
    this.order.column = columnKey;
    this.onOrderChanged.emit(this.order);
  }

  rowValueChanged(e) {
    this.onRowValueChanged.emit(e);
  }

  rowClicked(rowData) {
    if (this.go.rowClickAction && this.go.rowClickAction.enable) {
      this.onRowClicked.emit(rowData);
    }
  }

  isFilterDefault(val) {
    return val === FilterType.default;
  }

  isFilterDateRange(val) {
    return val === FilterType.dateRange;
  }

  isFilterYesNo(val) {
    return val == FilterType.yesNoRadio;
  }

  isFilterTypehead(val) {
    return val == FilterType.typeHead;
  }

  isFilterDropdown(c: NRGridColumn) {
    return c.filterType == FilterType.dropdown;
  }

  isFilterNumericRange(val) {
    return val == FilterType.numericRange;
  }

  typeheads = [];

  selectTypeheadItem($event, key) {
    this.rowFilter[key] = $event.item;
    this.filterChange();
  }

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;

  onDateSelection(date, filterKey) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate && this.toDate) {
      let bs = this.fromDate;
      let be = this.toDate;
      let s = new Date(bs.year, bs.month - 1, bs.day);
      let e = new Date(be.year, be.month - 1, be.day);
      this._rangeValue = format(s, 'dd/MM/yyyy') + ' - ' + format(e, 'dd/MM/yyyy');
      this.rangePicker.toggle();
      this.rowFilter[filterKey] = s.toISOString() + ',' + e.toISOString();
      this.filterChange();
    }
  }

  _rangeValue: string = '';

  rangeValue() {
    return this._rangeValue;
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }


  getFilterKey(c: NRGridColumn) {
    return c.filterKey ? c.filterKey : c.name;
  }

  rangeChanged(c) {
    let key = this.getFilterKey(c);
    let startKey = key + '___start';
    let endKey = key + '___end';

    let start = this.ranges[startKey];
    let end = this.ranges[endKey];

    this.rowFilter[key] = (start ? start : '') + '___' + (end ? end : '');
    this.filterChange();
  }

  limit = 10;

  limitChanged(limit) {
    this.limit = Number(limit.value ? limit.value : limit);
    if (this.options.dataSource) {
      this.options.dataSource.reload();
    }
    this.onLimitChanged.emit(this.limit);
  }

  loadLookupData(c: NRGridColumn) {
    if (!c.dropdownData) {
      c.dropdownDataService.subscribe(result => {
        c.dropdownData = _.map(result.data, function (x) {
          return {
            id: x.id,
            text: x.name
          };
        });
      });
    }
  }

  customButtonClick(button: NRGridButton, row: any) {
    this.onCustomButtonClicked.emit({
      button: button,
      data: row
    });
  }

  inlineEditorOn(cr: NRGridColumn, rowIndex: number, rowValue: any) {
    if (cr.type == NRGridColumnType.inlineEditor) {
      this.editingRows['row-' + rowIndex + '-col-' + cr.name] = {
        firstValue: rowValue[cr.name]
      };

      this.cdr.detectChanges();
      let e = document.getElementById('row-' + rowIndex + '-col-' + cr.name);
      if (e) {
        e.focus();
      }
    }
  }

  inlineEditorOff(cr: NRGridColumn, rowIndex: number, rowValue = undefined, toggleEvent = false, revert: boolean = false) {
    if (revert) {
      rowValue[cr.name] = this.editingRows['row-' + rowIndex + '-col-' + cr.name].firstValue;
    }
    this.editingRows['row-' + rowIndex + '-col-' + cr.name] = undefined;
    if (!revert && toggleEvent) {
      this.rowValueChanged({
        column: cr,
        row: rowValue
      });
    }
  }

}
