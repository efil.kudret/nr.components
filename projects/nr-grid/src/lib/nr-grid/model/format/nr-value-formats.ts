export enum NRValueFormats {
  default,
  euro,
  dolar,
  try,
  commaInt,
  percent,
  dateTime,
  date,
  boolean,
  decimal,
  number
}
