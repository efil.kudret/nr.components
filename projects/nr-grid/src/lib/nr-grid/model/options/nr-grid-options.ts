import {NRGridColumn} from '../column/nr-grid-column';
import {NRRowClickAction} from '../row/nr-row-click-action';
import {NRGridButton} from '../button/nr-grid-button';
import {NRDataSource} from '../data-source/nr-data-source';
import {NRButtonOption} from '../button/nr-button-option';

export class NRGridOptions {
  loading?: boolean;
  page?: number;
  limit?: number;
  summary?: any;
  rowCount?: number;
  data?: any[];
  columns: NRGridColumn[];
  filterRowEnabled?: boolean;
  actionButtons?: boolean;
  rowClickAction?: NRRowClickAction;
  buttons?: NRGridButton[];
  dataSource?: NRDataSource;
  actionButtonsOptions?: NRButtonOption;
  bootstrapTableClass?: string;
  pager?: boolean;
  parent?: any;
  hideSummary?: boolean;
}
