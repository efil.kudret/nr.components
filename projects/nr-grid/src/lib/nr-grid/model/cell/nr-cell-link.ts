export class NRCellLink {
  public tooltip?: string;
  public type?: string; // button or a
  public classes?: string;
  public icon?: string;
}
