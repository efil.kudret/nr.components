export enum FilterType {
  default,
  date,
  dateRange,
  yesNoRadio,
  typeHead,
  numericRange,
  dropdown
}
