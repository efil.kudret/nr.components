export class NRDataSource {
  loader: any;
  parent: any;
  reload?: any;
  usePromise?: boolean;
  manualLoading?: boolean;
}
