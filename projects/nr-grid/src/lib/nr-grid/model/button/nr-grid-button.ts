export class NRGridButton {
  public tooltip: string;
  public type: string;
  public icon?: string;
  public text?: string;
  public id: any;
  public visible?: any;
}
