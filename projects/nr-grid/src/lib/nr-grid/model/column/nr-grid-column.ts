import {FilterType} from '../filter/filter-type';
import {NRGridColumnType} from './nr-grid-column-type';
import {NRCellLink} from '../cell/nr-cell-link';
import {NRValueFormats} from '../format/nr-value-formats';
import {NRSelectOptions} from 'nr-select';

export class NRGridColumn {
  public constructor(init?: Partial<NRGridColumn>) {
    (Object as any).assign(this, init);
  }

  public title?: string;
  public name: string;
  public width?: any;
  public format?: any;
  public orderKey?: string;
  public filterType?: FilterType;
  public filterKey?: string;
  public filterDisable?: boolean;
  public alignment?: string;
  public type?: NRGridColumnType;
  public link?: NRCellLink;
  public formatter?: NRValueFormats;
  public denied?: boolean;
  public hideFromSummary?: boolean;
  public dropdownData?: any;
  public dropdownDataService?: any;
  public selectOptions?: NRSelectOptions;
  public inlineEditorActive?: boolean;
  public hide?: boolean;
}
