import {Pipe, PipeTransform} from '@angular/core';
import format from 'date-fns/format';
import {NRValueFormats} from '../nr-grid/model/format/nr-value-formats';


import * as _ from 'lodash';
import {currencies} from "../data/currency.symbol";

@Pipe({name: 'valueFormat'})
export class ValueFormatPipe implements PipeTransform {
  transform(value: any, type: NRValueFormats | string, currency?: string): string {

    let valueTypeStr = '';
    let baseSymbol;

    if (typeof type == 'string') {
      valueTypeStr = type;
    }


    if (currency) {
      let found = _.find(currencies, (x) => {
        return x && x.id && x.id == currency ? x : undefined;
      });
      if (found)
        baseSymbol =  found.symbol;
      else
        baseSymbol = currency;
    }
    else
      baseSymbol = '';

    let digits = 2;
    if (valueTypeStr) {
      switch (valueTypeStr) {
        default:
        case 'number':
          type = NRValueFormats.number;
          break;
        case 'bool':
          type = NRValueFormats.boolean;
          break;
        case 'dateTime':
          type = NRValueFormats.dateTime;
          break;
        case 'date':
          type = NRValueFormats.date;
          break;
        case 'decimal':
          type = NRValueFormats.decimal;
          break;
        case 'commaInt':
          type = NRValueFormats.commaInt;
          break;
      }
    }
    if (value == undefined && type != NRValueFormats.boolean) {
      return '';
    }


    switch (type) {
      default:
      case NRValueFormats.default:
      case NRValueFormats.percent:
      case NRValueFormats.commaInt:
      case NRValueFormats.decimal:
      case NRValueFormats.number:
        switch (type) {
          case NRValueFormats.percent:
            baseSymbol = ' %';
            break;
          case NRValueFormats.decimal:
            digits = 3;
            break;
          case NRValueFormats.number:
            digits = 2;
            break;
          default:
          case NRValueFormats.commaInt:
            digits = 0;
            break;
        }
        return value
          .toFixed(digits)
          .replace('.', ',')
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + ' ' + baseSymbol;
      case NRValueFormats.dateTime:
        return format(value, 'dd/MM/yyyy HH:mm:ss');
      case NRValueFormats.date:
        return format(value, 'dd/MM/yyyy');
      case NRValueFormats.boolean:
        return value ? 'Yes' : 'No';
    }
  }
}
