import { NgModule } from '@angular/core';
import {NRGridComponent} from './nr-grid/nr-grid.component';
import {ValueFormatPipe} from './pipes/value-format.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NrSelectModule} from "nr-select";



@NgModule({
  declarations: [NRGridComponent, ValueFormatPipe],
    imports: [
        NgbModule,
        CommonModule,
        FormsModule,
        NrSelectModule
    ],
  exports: [NRGridComponent, ValueFormatPipe]
})
export class NrGridModule {
}
