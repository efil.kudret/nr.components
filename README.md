# NRComponents

This repository contains angular components developed by NRTech 

# NRGrid

NR-Grid is basic a datagrid helper that uses Bootstrap and NgbBootstrap and developed for Angular applications. 

#Intallation
`npm install nr-grid`

# Basic Usage

**app.component.html**

`<nr-grid [options]="gridOptions"></nr-grid>`

**app.component.ts**

```
gridOptions: NRGridOptions = {

     columns: [
       {
         name: 'id',
         title: 'Id'
       },
       {
         name: 'name',
         title: 'Name'
       }
     ],
     data: [
       {id: 1, name: 'test 1'},
       {id: 2, name: 'test 2'},
       {id: 3, name: 'test 3'},
     ]
   };
```
