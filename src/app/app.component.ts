import {Component} from '@angular/core';
import {NRGridOptions} from '../../projects/nr-grid/src/lib/nr-grid/model/options/nr-grid-options';
import {NRSelectOptions} from '../../projects/nr-select/src/lib/model/nr-select-options';
import {HttpClient} from '@angular/common/http';
import {NRGridColumnType} from "../../projects/nr-grid/src/lib/nr-grid/model/column/nr-grid-column-type";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nr-components';

  constructor(public http: HttpClient) {
  }

  gridOptions: NRGridOptions = {
    columns: [
      {
        name: 'id',
        title: 'Id',
        width: '200px'
      },
      {
        name: 'name',
        title: 'Name'
      },
      {
        name: 'translation',
        title: 'Translation',
        type: NRGridColumnType.inlineEditor
      },
      {
        name: 'link',
        title: 'Link',
        type: NRGridColumnType.link
      }
    ],
    data: [
      {id: 1, name: 'test 1', translation: 'ascasc', link: 'Deneme'},
      {id: 2, name: 'test 2', translation: undefined, link: 'Deneme'},
      {id: 3, name: 'test 3', translation: undefined, link: 'Deneme'},
    ],
    filterRowEnabled: true
  };

  data = [
    {id: 1, name: 'test 1'},
    {id: 2, name: 'test 2'},
    {id: 3, name: 'test 3'},
  ];

  selectValue: number;
  selectOptions: NRSelectOptions = {
    displayMember: 'name',
    valueMember: 'id',
    hideLabel: false,
    searchable: true,
    allowClear: true
  };
  submitted: boolean = false;

  changeVal() {
    this.selectValue = 3;
  }

  selectValue2 = undefined;
  selectValue3 = undefined;

  selectOptions2: NRSelectOptions = {
    displayMember: 'full_name',
    displayTextFormat: (x) => {
      return `${x.full_name} ${x.html_url}`;
    },
    valueMember: 'id',
    hideLabel: false,
    allowClear: true,
    searchable: true,
    lazyLoad: true,
    dataSource: {
      parent: this,
      loader: this.load
    },
    searchAfterKeypress: true
  };

  load(p: AppComponent) {
    return p.http.get('https://api.github.com/repositories');
  }

  gridFilter: any = {};

  testFilter() {
    this.gridFilter.name = "deneme";
  }

  filterChangedOld(e) {
    console.log(e);
  }

  changed(e) {

  }

}
